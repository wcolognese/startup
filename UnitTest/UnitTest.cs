﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using StartUpLibrary;

namespace UnitTest
{
    [TestClass]
    public class UnitTest
    {
        StartUp startUp;
        string appName = "appTest";

        [TestInitialize]
        public void SetUp()
        {
            this.startUp = new StartUp(this.appName, "C:\\teste.exe");
        }

        [TestCleanup]
        public void TearDown()
       { 
            this.startUp.DeleteRegister();
        }

        [TestMethod]
        public void RegisterWorks()
        {
            this.startUp.RegisterOrRemove(StartUp.Action.Register);

            Assert.IsTrue(this.startUp.AlreadyRegister());
        }

        [TestMethod]
        public void RemoveWorks()
        {
            //O registro deve existir para ser removido
            this.startUp.RegisterOrRemove(StartUp.Action.Register);

            Assert.IsTrue(this.startUp.AlreadyRegister());

            this.startUp.RegisterOrRemove(StartUp.Action.Remove);

            Assert.IsFalse(this.startUp.AlreadyRegister());
        }

        [TestMethod]
        public void ReplaceCaseModificated()
        {
            this.startUp.RegisterOrRemove(StartUp.Action.Register);

            Assert.IsTrue(this.startUp.AlreadyRegister());

            this.startUp = new StartUp(this.appName, "X:\\teste.exe", true);

            this.startUp.RegisterOrRemove(StartUp.Action.Register);

            string newPath = this.startUp.GetRegistryValue().ToString();

            Assert.AreNotEqual("C:\\teste.exe", newPath);                
        }
    }
}
