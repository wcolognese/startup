# README #
------
  Registrar o sistema para inicializar juntamente com o Windows


## Usabilidade ##


* ### Usando Action ###

   >StartUp startUp = new StartUp("AppTest");


   >startUp.RegisterOrRemove(startUp.Action.Register);


* ### Usando Boolean ###

 >StartUp startUp = new StartUp("AppTest");

 >CheckBox ckBox = new CheckBox();

 >ckBox.Checked = true;

 >startUp.RegisterOrRemove(startUp.MapAction(ckBox.Checked));