﻿using Microsoft.Win32;
using System.IO;

namespace StartUpLibrary
{
    public class StartUp
    {
        public string AppName { get; set; }
        public string SubKey = "SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run";
        public FileInfo ExecutablePath { get; set; }
        RegistryKey rkApp = null;
        public bool ReplaceCaseExists;

        RegistryKey RkApp
        {
            get { return this.GetRegistryKeyApp(); }
            set { this.rkApp = value; }
        }

        public enum For
        {
            AllUsers,
            CurrentUser,
            LocalMachine
        };

        public enum Action
        {
            Register,
            Remove,
        };

        /// <summary>
        /// Passe por parametro o nome da aplicação, este será o nome que vai aparecer nos programas inicializados pelo windows ao iniciar.
        /// Caso queira expecificar o caminho do executavel, por padrao é pego da entrada do Assembly.
        /// </summary>
        /// <param name="appName">Nome da aplicação</param>
        /// <param name="executablePath">caminho do executavel</param>
        /// <param name="replaceCaseChanged">substitui o caminho do executavel caso modificado</param>
        public StartUp(string appName, string executablePath, bool replaceCaseChanged = false)
        {
            this.SetUp(appName, executablePath, replaceCaseChanged);
        }

        /// <summary>
        /// Passe por parametro o nome da aplicação, este será o nome que vai aparecer nos programas inicializados pelo windows ao iniciar.
        /// </summary>
        /// <param name="appName"></param>
        public StartUp(string appName, bool replaceCaseChanged = false)
        {
            this.SetUp(appName, System.Reflection.Assembly.GetEntryAssembly().Location, replaceCaseChanged);
        }

        private void SetUp(string appName, string executablePath, bool replaceCaseChanged)
        {
            AppName = appName;
            this.ExecutablePath = new FileInfo(executablePath);
            this.ReplaceCaseExists = replaceCaseChanged;
        }

        public bool AlreadyRegister()
        {
            return this.GetRegistryValue() != null ? true : false;
        }

        public object GetRegistryValue()
        {
            return RkApp.GetValue(AppName);
        }

        public void DeleteRegister()
        {
            RkApp.DeleteValue(AppName, false);
        }

        public void Register()
        {
            if((!this.AlreadyRegister()) || (this.GetRegistryValue() != this.ExecutablePath))
                RkApp.SetValue(this.AppName, this.ExecutablePath);
        }

        /// <summary>
        /// Register para registrar caso não esteje registrado.
        /// Remove para retirar o registro caso esteje registrado.
        /// </summary>
        /// <param name="action"></param>
        public void RegisterOrRemove(Action action = Action.Register)
        {
            if (Action.Register == action)
            {
                this.Register();
            }
            else if (Action.Remove == action && this.AlreadyRegister())
            {
                this.DeleteRegister();
            }
        }


        /// <summary>
        /// Facilita o mapeamento da ação de boleano para Action.
        /// </summary>
        /// <param name="action"></param>
        /// <returns></returns>
        public Action MapAction(bool action)
        {
            return action ? Action.Register : Action.Remove;
        }

        public RegistryKey GetRegistryKeyApp()
        {
            if (this.rkApp == null)
                return this.GetForCurrentUser();
            else
                return this.rkApp;
        }

        public void SetRegistryKey(For _for)
        {
            switch (_for)
            { 
                case For.AllUsers:
                        this.RkApp = this.GetForAllUsers();
                    break;
                case For.CurrentUser:
                        this.RkApp = this.GetForCurrentUser();
                    break;
                case For.LocalMachine:
                        this.RkApp = this.GetForLocalMachine();
                    break;
            }
        }

        protected RegistryKey GetForAllUsers()
        {
            return Registry.Users.OpenSubKey(SubKey, true);
        }

        protected RegistryKey GetForCurrentUser()
        {
            return Registry.CurrentUser.OpenSubKey(SubKey, true);
        }

        protected RegistryKey GetForLocalMachine()
        {
            return Registry.LocalMachine.OpenSubKey(SubKey, true);
        }
    }
}
